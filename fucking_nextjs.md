fucking nextjs


Pre-renders
Next.js generates HTML for each page in advance, instead of having it all done by client-side JavaScript. 
Pre-rendering can result in better performance and SEO.

Each generated HTML is associated with minimal JavaScript code necessary for that page. 
When a page is loaded by the browser, its JavaScript code runs and makes the page fully interactive. 
(This process is called hydration.)




- Static Generation (Recommended): The HTML is generated at build time and will be reused on each request.
- Server-side Rendering: The HTML is generated on each request




Static Generation without data : 

function About() {
  return <div>About</div>
}

export default About


Static Generation with data : 

Your page content depends on external data: Use getStaticProps.
Your page paths depend on external data: Use getStaticPaths (usually in addition to getStaticProps).


When should I use Static Generation?

 - Marketing pages
 - Blog posts and portfolios
 - E-commerce product listings
 - Help and documentation



Image Component and Image Optimization

 - Improved Performance: Always serve correctly sized image for each device, using modern image formats.
 - Visual Stability: Prevent Cumulative Layout Shift automatically.
 - Faster Page Loads: Images are only loaded when they enter the viewport, with optional blur-up placeholders
 - Asset Flexibility: On-demand image resizing, even for images stored on remote servers


Image Sizing

One of the ways that images most commonly hurt performance is through layout shift, where the image pushes other elements around on the page as it loads in. This performance problem is so annoying to users that it has its own Core Web Vital, called Cumulative Layout Shift. The way to avoid image-based layout shifts is to always size your images. This allows the browser to reserve precisely enough space for the image before it loads.


What if I don't know the size of my images?

The fill layout mode allows your image to be sized by its parent element. Consider using CSS to give the image's parent element space on the page, then using the objectFit property with fill, contain, or cover, along with the objectPosition property to define how the image should occupy that space.

When using layout='fill', the parent element must have position: relative

This is necessary for the proper rendering of the image element in that layout mode.

When using layout='responsive', the parent element must have display: block







